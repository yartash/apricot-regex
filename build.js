const fs = require('fs-extra');
const path = require('path');
const { exec, spawn } = require('child_process');

var buildPath = path.resolve(process.cwd(), 'build');

if (fs.existsSync(buildPath)) {
    fs.removeSync(buildPath);
}

fs.mkdirSync(buildPath);
process.chdir(buildPath);

console.log('Build start.');

exec('git clone https://gitlab.com/yartash/apricot-regex-cmd.git', error => {
    if (!error) {
        process.chdir(path.join(buildPath, 'apricot-regex-cmd'));
        let restore = spawn('dotnet', ['restore', 'ApricotRegexCommand.sln']);

        restore.stderr.on('data', function (data) {
            console.log(data.toString());
        });

        restore.stdout.on('data', function (data) {
            console.log(data.toString());
        });

        restore.on('close', function () {
            var build = spawn('dotnet', ['build', 'ApricotRegexCommand.sln']);

            build.stderr.on('data', function (data) {
                console.log(data.toString());
            });

            build.stdout.on('data', function (data) {
                console.log(data.toString());
            });

            build.on('close', function () {
                fs.copy(
                    path.join(buildPath, 'apricot-regex-cmd', 'ApricotRegexCommand', 'bin', 'Debug', 'netcoreapp1.1'),
                    path.join(buildPath, '..', 'bin')
                );

                console.log('Build finish.');
            });
        });
    }
});