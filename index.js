const fs = require('fs');
const { exec } = require('child_process');
const path = require('path');

const _ = require('lodash');
const uuidv4 = require('uuid/v4');

class ApricotRegex {
    static get flagNone() {
        return 0;
    }

    static get flagIgnoreCase() {
        return 1;
    }

    static get flagMultiline() {
        return 2;
    }

    static get flagExplicitCapture() {
        return 4;
    }

    static get flagSingleline() {
        return 16;
    }

    static get flagIgnorePatternWhitespace() {
        return 32;
    }

    static get flagRightToLeft() {
        return 64;
    }

    static get flagCultureInvariant() {
        return 512;
    }

    get pattern() {
        return this._pattern;
    }

    get flags() {
        return this._flags;
    }

    set pattern(value) {
        this._pattern = value;
    }
    set flags(value) {
        this._flags = value;
    }

    constructor(pattern, flags = ApricotRegex.flagNone) {
        if (!_.isNumber(flags)) {
            throw new Error('Invalid type of flag argument. A flag can only be number');
        }

        this._pattern = pattern;
        this._flags = flags;
    }

    test(content) {
        return new Promise(
            (resolve, reject) => {
                this
                    ._runCommand(content)
                    .then(
                        result => {
                            if (result.status == 'success') {
                                resolve(true);
                            } else if (result.status == 'error' && result.message == 'Not matching') {
                                resolve(false);
                            } else {
                                reject(result);
                            }
                        },
                        error => {
                            reject(error);
                        }
                    )
            }
        );
    }

    match(content) {
        return new Promise(
            (resolve, reject) => {
                this
                    ._runCommand(content)
                    .then(
                        result => {
                            if (result.status == 'success') {
                                resolve(result.matches);
                            } else if (result.status == 'error' && result.message == 'Not matching') {
                                resolve([]);
                            } else {
                                reject(result);
                            }
                        },
                        error => {
                            reject(error);
                        }
                    )
            }
        );
    }

    split(content) {
        return new Promise(
            (resolve, reject) => {
                this
                    ._runCommand(content)
                    .then(
                        result => {
                            if (result.status == 'success') {
                                let start = 0;
                                let index = 0;
                                let length = content.length;
                                let parts = [];

                                while (start < length) {
                                    let match = result.matches[index];
                                    parts.push(
                                        content
                                            .substring(start, match.index)
                                    );
                                    start = match.index + match.length;
                                    index++;
                                }
                                resolve(parts);
                            } else if (result.status == 'error' && result.message == 'Not matching') {
                                resolve([content]);
                            } else {
                                reject(result);
                            }
                        },
                        error => {
                            reject(error);
                        }
                    )
            }
        );
    }

    replace(content, replace) {
        let result = {
            content: []
        };
        let promise = new Promise(
            (resolve, reject) => {
                result.resolve = resolve;
                result.reject = reject;
            }
        );

        this
            ._runCommand(content)
            .then(
                matches => {
                    if (matches.status == 'success') {
                        let start = 0;
                        let parts = [];
                        let last = _.last(matches.matches);

                        _
                            .each(
                                matches.matches,
                                match => {
                                    if (start != match.index) {
                                        parts.push({
                                            value: content.substring(start, match.index),
                                            match: false
                                        });
                                    }

                                    parts.push({
                                        value: match.groups,
                                        match: true
                                    });
                                    start = match.index + match.length;
                                }
                            );

                        if (last.index + last.length < content.length) {
                            parts.push({
                                value: content.substr(last.index + last.length),
                                match: false
                            });
                        }

                        function iteration(parts) {
                            if (!parts.length) {
                                result.resolve(
                                    result
                                        .content
                                        .join('')
                                );

                                return;
                            }

                            let part = parts[0];

                            if (part.match) {
                                if (_.isString(replace)) {
                                    nextStep(replace);
                                } else if (
                                    replace.constructor.name &&
                                    replace.constructor.name == 'GeneratorFunction'
                                ) {
                                    let generatorPromiseResolve;

                                    function execute(generator, yieldValue) {
                                        let next = generator.next(yieldValue);

                                        if (!next.done) {
                                            if (
                                                next.value.constructor
                                            ) {
                                                switch (next.value.constructor.name) {
                                                    case 'Promise':
                                                        next.value.then(
                                                            res => {
                                                                execute(generator, res);
                                                            },
                                                            err => {
                                                                generator.throw(err);
                                                            }
                                                        );
                                                        break;
                                                    case 'Number':
                                                        execute(generator, next.value);
                                                        break;
                                                }
                                            }
                                        } else {
                                            generatorPromiseResolve(next.value);
                                        }
                                    }

                                    new Promise(
                                        resolve => {
                                            generatorPromiseResolve = resolve;
                                        }
                                    )
                                        .then(
                                            res => {
                                                nextStep(res);
                                            }
                                        );
                                    execute(replace(part.value));

                                } else if (_.isFunction(replace)) {
                                    let data = replace(part.value);

                                    if (data instanceof Promise) {
                                        data
                                            .then(
                                                res => {
                                                    nextStep(res);
                                                }
                                            )
                                    } else {
                                        nextStep(data);
                                    }
                                }
                            } else {
                                nextStep(part.value);
                            }
                        }

                        function nextStep(value)  {
                            result
                                .content
                                .push(
                                    value
                                );

                            parts.shift();
                            iteration(parts);
                        }

                        iteration(parts);
                    } else if (matches.status == 'error' && matches.message == 'Not matching') {
                        result.resolve(content);
                    } else {
                        result.reject(result);
                    }
                },
                error => {
                    result.reject(error);
                }
            );

        return promise;
    }

    _runCommand(content) {
        return new Promise(
            (resolve, reject) => {
                content = content
                    .replace(/\n/g, '&#10;')
                    .replace(/\r/g, '&#11;')
                    .replace(/"/g, '&#12;');
                this._pattern = this
                    ._pattern
                    .replace(/\n/g, '&#10;')
                    .replace(/\r/g, '&#11;')
                    .replace(/"/g, '&#12;');

                let dllPath = path.join(__dirname, '/bin/ApricotRegexCommand.dll');
                let command = '';

                if (content.length >= 5000) {
                    let tempFile = path.join(__dirname, '/bin/', `${ uuidv4() }.txt`);
                    fs.writeFileSync(tempFile, content, 'utf8');
                    command = `dotnet ${ dllPath } "-p:${ this._pattern }" "-c:${ tempFile }" -f:${ this._flags } -o`;
                } else {
                    command = `dotnet ${ dllPath } "-p:${ this._pattern }" "-c:${ content }" -f:${ this._flags }`;
                }

                exec(
                    command,
                    (error, stdout, stderr) => {
                        if (error || stderr) {
                            reject(error || stderr);
                        }

                        try {
                            let result = JSON.parse(stdout);

                            resolve(result);
                        } catch (err) {
                            reject(err);
                        }
                    }
                )
            }
        )
    }
}

module.exports = ApricotRegex;